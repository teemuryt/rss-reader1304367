package com.example.teemu.rssreader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Teemu on 12.10.2015.
 */
public class FeedAdapterItems extends ArrayAdapter <FeedItem> {

    private ArrayList<FeedItem> feeditems;
    private Context context;

    public FeedAdapterItems(Context context, int resource, ArrayList<FeedItem> feedItems){
        super (context, resource, feedItems);
        this.feeditems = feedItems;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View targetView = convertView;
        if (targetView == null){
            LayoutInflater li = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            targetView = li.inflate(R.layout.row_layout, null);
        }

        FeedItem thisFeedItem = feeditems.get(position);
        if(thisFeedItem != null){
            TextView tv = (TextView)targetView.findViewById(R.id.title);
            tv.setText(thisFeedItem.getFeedItemTitle());
            tv = (TextView)targetView.findViewById(R.id.pubDate);
            tv.setText(thisFeedItem.getFeedItemDate());
            tv = (TextView)targetView.findViewById(R.id.link);
            tv.setText((thisFeedItem.getFeedItemLink()));
        }
        return targetView;
    }
}
