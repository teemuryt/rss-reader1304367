package com.example.teemu.rssreader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

/**
 * Created by Teemu on 12.10.2015.
 */
public class FeedItemView extends AppCompatActivity implements Observer {
    private DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    RelativeLayout mProfileBox;
    private ActionBarDrawerToggle mDrawerToggle;
    private int passedFeedPosition;
    private FeedAdapterItems feedAdapterItems;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle extras = getIntent().getExtras();
        this.passedFeedPosition = (Integer) extras.get("passedFeed");
        Log.e("passed","position " + passedFeedPosition);
        feedAdapterItems = new FeedAdapterItems(this, R.layout.row_layout, User.getInstance().getFeeds().get(passedFeedPosition).getFeedItems());
        final ListView myLV = (ListView) findViewById(R.id.rss_feed);
        myLV.setAdapter(feedAdapterItems);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerList = (ListView) findViewById(R.id.feedilisti);
        mProfileBox = (RelativeLayout) findViewById(R.id.profileBox);
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        final Button addbtn = (Button) findViewById(R.id.drawerBtnAdd);
        final Button refreshbtn = (Button) findViewById(R.id.refreshBtn);
        final Button favBtn = (Button) findViewById(R.id.fav_button);
        final Button readBtn = (Button) findViewById(R.id.readBtn);
        User.getInstance().registerObserver(FeedItemView.this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close){
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        addbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(FeedItemView.this);

                alert.setTitle("Add a new feed");
                alert.setMessage("input URL");

// Set an EditText view to get user input
                final EditText input = new EditText(FeedItemView.this);
                alert.setView(input);

                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String m_Text = input.getText().toString();
                        PullParserXml ppx = new PullParserXml(m_Text);
                        Thread tf = new Thread(ppx);
                        tf.start();

                        // Do something with value!
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        });
        refreshbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                update();
            }
        });

        myLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final FeedItem tempFeedItem = (FeedItem) (myLV.getItemAtPosition(position));
                Intent intent = new Intent(FeedItemView.this, Pop.class);
                intent.putExtra("passedFeedItem", position);
                intent.putExtra("passedFeed", passedFeedPosition);
                startActivity(intent);
            }


        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void update(){
        this.runOnUiThread(new Runnable() {
            public void run() {
                feedAdapterItems.notifyDataSetChanged();

                //final TextView ikkuna = (TextView) findViewById(R.id.rss_feed);
            }
        });
    }
}
