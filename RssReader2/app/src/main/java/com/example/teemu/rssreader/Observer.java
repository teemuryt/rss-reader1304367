package com.example.teemu.rssreader;

/**
 * Created by Teemu on 2.10.2015.
 */
public interface Observer {
    void update();
}
