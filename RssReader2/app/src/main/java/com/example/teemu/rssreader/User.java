package com.example.teemu.rssreader;

import android.app.AlertDialog;
import android.widget.EditText;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Teemu on 1.10.2015.
 */
public class User{
    private ArrayList<Observer> observers = new ArrayList<Observer>();
    private ArrayList<FeedItem> favItems = new ArrayList<FeedItem>();
    private ArrayList<FeedItem> readItems = new ArrayList<FeedItem>();
    private ArrayList <Feed> feeds = new ArrayList<Feed>();
    public void registerObserver(Observer o){
        observers.add(o);
    }
    public void unregisterObserver(Observer o){
        observers.remove(o);
    }
    public void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }
    private static final User INSTANCE = new User();
    private User() {
        //Feed f1 = new Feed();
        //f1.addFeedTitle("testi");
        //addToFeeds(f1);
    }
    public static User getInstance() {
        return INSTANCE;
    }
    public ArrayList <Feed> getFeeds(){
        return feeds;
    }
    public ArrayList <FeedItem> getFavItems(){
        return this.favItems;
    }
    public void addToFeeds(Feed feedi){
        feeds.add(feedi);
        notifyAllObservers();
    }
    public void deleteFromFeeds(Feed f){
        feeds.remove(f);
        notifyAllObservers();
    }
    public void favFI(FeedItem f){
        favItems.add(f);
        notifyAllObservers();
    }
    public void readFi(FeedItem f) {readItems.add(f);
        notifyAllObservers();
    }
}
