package com.example.teemu.rssreader;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Observer {
    private DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    RelativeLayout mProfileBox;
    private ActionBarDrawerToggle mDrawerToggle;
    private FeedAdapter feedadapter;
    DrawerListAdapter draweradapter;// = new DrawerListAdapter(MainActivity.this, choices);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User.getInstance().registerObserver(this);
        setContentView(R.layout.activity_main);
        PullParserXml ppx = new PullParserXml("http://www.iltasanomat.fi/rss/tuoreimmat.xml");
        Thread t1 = new Thread(ppx);
        t1.start();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerList = (ListView) findViewById(R.id.feedilisti);
        mProfileBox = (RelativeLayout) findViewById(R.id.profileBox);
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        final ListView myLV = (ListView) findViewById(R.id.rss_feed);
        final Button addbtn = (Button) findViewById(R.id.drawerBtnAdd);
        final Button refreshbtn = (Button) findViewById(R.id.refreshBtn);
        final Button favBtn = (Button) findViewById(R.id.fav_button);
        final Button readBtn = (Button) findViewById(R.id.readBtn);
        Log.e("print","FeedList" + User.getInstance().getFeeds().size());
        feedadapter = new FeedAdapter(this, R.layout.drawer_item, User.getInstance().getFeeds());
        mDrawerList.setAdapter(draweradapter = new DrawerListAdapter(MainActivity.this, User.getInstance().getFeeds()));
        myLV.setAdapter(feedadapter);
        /*mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int editedPosition = position + 1;
                Toast.makeText(MainActivity.this, "You selected item " + editedPosition, Toast.LENGTH_SHORT).show();
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });*/
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close){
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        addbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                alert.setTitle("Add a new feed");
                alert.setMessage("input URL");

// Set an EditText view to get user input
                final EditText input = new EditText(MainActivity.this);
                alert.setView(input);

                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String m_Text = input.getText().toString();
                        PullParserXml ppx = new PullParserXml(m_Text);
                        Thread tf = new Thread(ppx);
                        tf.start();

                        // Do something with value!
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        });
        favBtn.setOnClickListener(new View.OnClickListener(){
                                     public void onClick(View v){
                                         Intent intent = new Intent(getApplicationContext(), Favorites.class);
                                         startActivity(intent);
                                     }
                                  });
        refreshbtn.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View v) {
                                              update();
                                          }
                                      });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Log.e("pull parser", "created" + ppx);
        myLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this); //THIS IS ALERTER, CLICK ON FEEDITEM AND GET SMALL ALERT POPUP WITH LINK OR FAVORITE OPTION.
                final Feed tempfd = (Feed) (myLV.getItemAtPosition(position));
                //alert.setPositiveButton("LINK", new DialogInterface.OnClickListener() {
                //public void onLongClick(DialogInterface dialog, int whichButton) {
                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempfeed.getFeedItemLink()));
                //startActivity(browserIntent);
                // }
                //});
                //alert.setNegativeButton("FAVORITE", new DialogInterface.OnClickListener() {
                //public void onClick(DialogInterface dialog, int whichButton) {
                //User.getInstance().favFI(tempfeed.getFeedItem());
                //}
                //});
                //alert.show();

                /*startActivity(new Intent*/
                Intent intent = new Intent(getApplicationContext(), FeedItemView.class);
                intent.putExtra("passedFeed", position);
                //Intent intent = new Intent(getApplicationContext(), SingleFeedView.class);
                //intent.putExtra("passedFeedItem", position);
                startActivity(intent);
                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempfeed.getFeedItemLink()));
                //startActivity(browserIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update() {


        //final TextView ikkuna = (TextView) findViewById(R.id.rss_feed);
        this.runOnUiThread(new Runnable() {
            public void run() {
                feedadapter.notifyDataSetChanged();

                //final TextView ikkuna = (TextView) findViewById(R.id.rss_feed);
            }
        });

    }


    /*AlertDialog.Builder alert = new AlertDialog.Builder(this);

alert.setTitle("Add a new feed");
alert.setMessage("input URL");

// Set an EditText view to get user input
final EditText input = new EditText(this);
alert.setView(input);

alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
public void onClick(DialogInterface dialog, int whichButton) {
PullParserXml ppx = new PullParserXml(input);
Thread tf = new Thread(ppx)
tf.start();
  // Do something with value!
  }
});*/


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

}