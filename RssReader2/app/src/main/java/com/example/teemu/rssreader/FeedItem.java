package com.example.teemu.rssreader;

/**
 * Created by Teemu on 29.9.2015.
 */
public class FeedItem {
    private String title;
    private String date;
    private String linkki;
    private String channel;
    private String description;
    public FeedItem(){
        this.title = title;
        this.date = date;
        this.linkki = linkki;
    }
    public void addFeedItemTitle(String title){
        this.title = title;
    }

    public void addFeedItemLinkki(String linkki){
        this.linkki = linkki;
    }

    public void addFeedItemDate(String date){
        this.date = date;
    }
    public String getFeedItemTitle(){
        return title;
    }
    public String getFeedItemDate (){
        return date;
    }public String getFeedItemLink(){
        return linkki;
    }
    public void setFiChannel(String chanel){
        this.channel = chanel;
    }
    public String getFiChannel(){
        return channel;
    }
    public void addFiDescription(String desc){
        this.description = desc;
    }
    public FeedItem getFeedItem(){
        return this;
    }
}
