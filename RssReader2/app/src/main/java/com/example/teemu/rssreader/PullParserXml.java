package com.example.teemu.rssreader;

/** Pullparser for the rss site gotten from feed object.
 * Created by Teemu on 25.9.2015.
 */
import android.util.Log;
import android.util.Xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PullParserXml implements Runnable {
    private Feed fd;
    private FeedItem fi;
    private XmlPullParser xpp;
    private InputStream is;
    private URL urli;
    private String currentText;
    public PullParserXml(String u){
        try {
            this.urli = new URL(u);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    //public PullParserXml(Feed feedi) {
        //this.feed = feedi;

        /*try {
            is = this.feedi.getAddress().openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void run() {
        try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            currentText = null;
            fi = null;
            fd = null;
        xpp.setInput(this.urli.openConnection().getInputStream(), null);
        Log.e("input","stream");
            int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {           //loops until the end of the document
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        fi = new FeedItem();
                        Log.e("feeditem","luotu");
                    } else if (xpp.getName().equalsIgnoreCase("channel")) {
                        fd = new Feed();
                        Log.e("FeedObjekti","luotu");
                        User.getInstance().addToFeeds(fd);
                    }
                    break;
                case XmlPullParser.TEXT:
                    currentText = xpp.getText();
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        fd.addToFeedItemList(fi);
                        fi.setFiChannel(fd.getFeedName());
                    } else if (xpp.getName().equalsIgnoreCase("title") && fi == null) {
                        fd.addFeedName(currentText);
                    } else if (xpp.getName().equalsIgnoreCase("title") && fi != null) {
                        fi.addFeedItemTitle(currentText);
                    } else if (xpp.getName().equalsIgnoreCase("description") && fi != null) {
                        fi.addFiDescription(currentText);
                    } else if (xpp.getName().equalsIgnoreCase("pubDate") && fi != null) {
                        fi.addFeedItemDate(currentText);
                    } else if (xpp.getName().equalsIgnoreCase("link") && fi != null) {
                        fi.addFeedItemLinkki(currentText);
                    }
                    break;
                default:
                    break;
            }
            eventType = xpp.next();
        }
        }
        catch (XmlPullParserException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        User.getInstance().notifyAllObservers();
    }




}
            /*if (eventType == XmlPullParser.START_TAG) {
                if(xpp.getName().equals("channel")) {                //checks for channel in xml file
                    Log.e("start", "tag");

                    //fd = new Feed(urli);                            //Creates the feed, and gives it the necessary information with xpp
                    if (xpp.getName().equalsIgnoreCase("title")) {
                        fd.addFeedTitle(xpp.nextText());
                        Log.e("title", "tallennettu");
                    } else if (xpp.getName().equalsIgnoreCase("link")) {
                        fd.addFeedLinkki(xpp.nextText());
                        Log.e("Linkki", "tallennettu");
                    } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                        fd.addFeedDate(xpp.nextText());
                        Log.e("pubDate", "tallennettu");
                    }
                }
                else if (xpp.getName().equalsIgnoreCase("item")) {            //Here's where it checks for the item from xml file
                        fi = new FeedItem();                        //creates a new FeedItem and also gives it the necessary information(Loops around multiple times to get everything it needs.
                        Log.e("new","feed item");}
                else if (xpp.getName().equalsIgnoreCase("title")) {
                        this.fi.addFeedItemTitle(xpp.nextText());
                        Log.e("title","tallennettu");}
                else if (xpp.getName().equalsIgnoreCase("link")) {
                        fi.addFeedItemLinkki(xpp.nextText());
                        Log.e("Linkki", "tallennettu");}
                else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                        fi.addFeedItemDate(xpp.nextText());
                        Log.e("pubDate", "tallennettu");}
                }
            else if (eventType == XmlPullParser.END_TAG) {          //Ends the FeedItem adding and adds it to the list
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        fd.addToFeedItemList(fi);
                        Log.e("EndTag feedlist", "EndTag feedlist");
                    } else if (xpp.getName().equalsIgnoreCase("channel")) {
                        Log.e("channel", "ended");
                    }
                }
            eventType = xpp.next();*/




