package com.example.teemu.rssreader;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
/**
 * Created by Teemu on 5.10.2015.
 */
public class Pop extends Activity {
    private int passedPosition;
    private int passedFeedPosition;
        @Override
    protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.popupwindow);

            Bundle extras = getIntent().getExtras();
            this.passedPosition = (Integer)extras.get("passedFeedItem");
            this.passedFeedPosition = (Integer)extras.get("passedFeed");

    DisplayMetrics dm = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(dm);

    int width = dm.widthPixels;
    int height = dm.heightPixels;

    getWindow().setLayout((int) (width * .8), (int) (height * .4));
    Button linkbtn = (Button)findViewById(R.id.link_button);
    linkbtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(User.getInstance().getFeeds().get(passedFeedPosition).getFeedItems().get(passedPosition).getFeedItemLink()));
            startActivity(browserIntent);
        }
    });

            Button favbtn = (Button)findViewById(R.id.fav_button);
            favbtn.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    User.getInstance().favFI(User.getInstance().getFeeds().get(passedFeedPosition).getFeedItems().get(passedPosition).getFeedItem());
                }
            });
}


}
