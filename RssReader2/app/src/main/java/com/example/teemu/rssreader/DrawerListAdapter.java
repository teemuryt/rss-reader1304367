package com.example.teemu.rssreader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Teemu on 8.10.2015.
 */
public class DrawerListAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<Feed> feeds;

    public DrawerListAdapter(Context context, ArrayList<Feed> feeds) {
        mContext = context;
        this.feeds = feeds;
    }

    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public Object getItem(int position) {
        return feeds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drawer_item, null);
        }
        else {
            view = convertView;
        }

        TextView titleView = (TextView) view.findViewById(R.id.feedtitle);
        //TextView subtitleView = (TextView) view.findViewById(R.id.subTitle);
        //ImageView iconView = (ImageView) view.findViewById(R.id.icon);

        //titleView.setText( mNavItems.get(position).mTitle );
        titleView.setText(feeds.get(position).getFeedName());
        //subtitleView.setText( mNavItems.get(position).mSubtitle );
        //iconView.setImageResource(mNavItems.get(position).mIcon);

        return view;
    }
}
