package com.example.teemu.rssreader;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Teemu on 1.10.2015.
 */
public class FeedAdapter extends ArrayAdapter<Feed>{

        private ArrayList<Feed> feeds;
        private Context context;

        public FeedAdapter(Context context, int resource, ArrayList<Feed> feedz){
            super (context, resource, feedz);
            this.feeds = feedz;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View targetView = convertView;
            if (targetView == null){
                LayoutInflater li = (LayoutInflater)context.getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                targetView = li.inflate(R.layout.drawer_item, null);
            }

            Feed thisFeed = feeds.get(position);
            if(thisFeed != null){
                TextView tv = (TextView)targetView.findViewById(R.id.feedtitle);
                tv.setText(thisFeed.getFeedName());
                //tv = (TextView)targetView.findViewById(R.id.pubDate);
                //tv.setText(thisFeedItem.getFeedItemDate());
            }
            return targetView;
        }
    }